<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ["uses" => "PageController@home", "as" => "home"]);
Route::get('/roof', ["uses" => "PageController@roof", "as" => "roof"]);
Route::get('/kitchen', ["uses" => "PageController@kitchen", "as" => "kitchen"]);