<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Theme;
use App\ActiveTheme;

class PageController extends Controller
{
    public function home()
    {
    	return view('welcome');
    }

    public function roof(Request $request)
    {
    	if($request->has('theme'))
    	{
    		$newTheme = new ActiveTheme;

    		if($request->theme)
    		{
    			$theme = Theme::find($request->theme);

    			if($theme)
    			{
    				$newTheme->theme_id = $theme->id;
    				$newTheme->save();

    				return array("status" => true); 
    			}
    			
    		}

    		$newTheme->theme_id = null;
    		$newTheme->custom_color = $request->color;
    		$newTheme->custom_led_color = $request->led_color;
    		$newTheme->save();

    		return array("status" => true);
    	}

    	if($request->has('update'))
    	{
    		$currentTheme = ActiveTheme::all()->last();

    		if($currentTheme->theme_id)
    		{
    			return array(
    				"media"	=> $currentTheme->theme->media,
    				"led"	=> $currentTheme->theme->led_color,
    				"custom"	=> false
    			);
    		}

    		return array(
				"media"	=> $currentTheme->custom_color,
				"led"	=> $currentTheme->custom_led_color,
				"custom"	=> true
			);
    	}


    	return view('roof');
    }
}
