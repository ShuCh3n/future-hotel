<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveTheme extends Model
{
    public function theme()
    {
    	return $this->belongsTo(Theme::class);
    }
}
