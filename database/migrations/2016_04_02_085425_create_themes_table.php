<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media');
            $table->string('led_color');
            $table->timestamps();
        });

        DB::table('themes')->insert([
            [
                'media'         =>  'rotterdam.mp4',
                'led_color'     =>  '0'
            ],
            [
                'media'         =>  'newyorkcity.mp4',
                'led_color'     =>  '0'
            ],
            [
                'media'         =>  'sky.mp4',
                'led_color'     =>  '0'
            ],
            [
                'media'         =>  'skyscrapers.mp4',
                'led_color'     =>  '0'
            ],
            [
                'media'         =>  'trees.mp4',
                'led_color'     =>  '0'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('themes');
    }
}
