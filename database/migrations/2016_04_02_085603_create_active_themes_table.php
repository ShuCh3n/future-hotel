<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_themes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theme_id')->unsigned()->nullable();

            //In case user wants custom solid color
            $table->string('custom_color');
            $table->string('custom_led_color');
            
            $table->timestamps();

            $table->foreign('theme_id')
                ->references('id')
                ->on('themes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('active_themes');
    }
}
