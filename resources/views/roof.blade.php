@extends('body')

@section('js')
	<script type="text/javascript" src="/js/roof.js"></script>
@stop

@section('content')
	<video id="mediasource" width="100%" controls autoplay muted loop>
		<source type="video/mp4">
	</video>
@stop